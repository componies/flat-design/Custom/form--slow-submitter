(function ($) {
  Drupal.behaviors.submitFormLoader = {
    attach: function (context, settings) {
      // For this javascript to work, you need to add the class-form--slow-submitter
      // to your form, and include this library.
      const $form = $('.form--slow-submitter');

      $form.once('fakeSlowSubmit').each(function () {
        $form.find('button[type=submit]').removeClass('js-loading');
        $form.submit(function(e) {
          // Get the clicked submit button element
          let val = $(this).find('button[type=submit][lastclicked=true]');
          val.addClass('js-loading');
        });

        $form.find('button[type=submit]', context).click(function() {
          $('button[type=submit]', $(this).parents('form')).removeAttr('lastclicked');
          $(this).attr('lastclicked', 'true');
        });
      });
    }
  };
})(jQuery);
